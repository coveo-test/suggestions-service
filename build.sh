#!/bin/sh
echo "Login to ECR Repository"
$(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
echo "Preparation task"
echo "Build Docker Image"
docker build -t $CONTAINER_NAME .
echo "Push to ECR Repository"
docker tag $CONTAINER_NAME:latest $AWS_ECR_REPOSITORY/$CONTAINER_NAME:latest
docker push $AWS_ECR_REPOSITORY/$CONTAINER_NAME:latest