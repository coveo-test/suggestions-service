from setuptools import setup, find_packages
setup(
    setup_requires=["pytest-runner"],
    tests_require=["pytest", "pytest-asyncio", "pytest-aiohttp", "pytest-toolbox", "pytest-cov"],
    name="Suggestions service",
    version="0.1.0",
    packages=find_packages(),
)
