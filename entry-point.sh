echo "Docker entry point"
gunicorn -w 2 -b :8000 app.app:app --worker-class aiohttp.worker.GunicornWebWorker --access-logfile '-' --log-level debug