from typing import Tuple

from geopy import distance as geo_distance


class GeoDistance:

    def __init__(self):
        self.max_distance = self.distance((0.0, 0.0), (0.0, 180.0))[0]

    @staticmethod
    def distance(pos_a: Tuple[float, float], pos_b: Tuple[float, float]) -> Tuple[float, float]:
        distance = geo_distance.distance(pos_a, pos_b)
        return distance.km, distance.miles

    def normalized_distance(self, pos_a: Tuple[float, float], pos_b: Tuple[float, float]) -> float:
        return 1.0 - min(max(self.distance(pos_a, pos_b)[0] / self.max_distance, 0.0), 1.0)
