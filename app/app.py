import asyncio
import os

import aiohttp_cors
from aiohttp import web
from aiohttp.web_app import Application
from aiohttp_swagger import setup_swagger

from app.city_database import CityDatabase
from app.geodistance import GeoDistance
from app.load_json_file import load_json_file
from app.model.city import CityModel
from app.trie import Trie

from app.app_routes import routes, invalid_route


async def initialize(web_app: Application):
    data_source_url = os.environ["CITY_DATA_SOURCE_URL"]
    response = await load_json_file(data_source_url)
    city_models = [CityModel.new(x) for x in response]
    trie = Trie()
    trie.insert(city_models)

    database = CityDatabase(city_models)
    web_app.update(
        trie=trie,
        database=database,
        geo_distance=GeoDistance()
    )


def create_app(debug: bool = False):
    web_app = web.Application(debug=debug)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(initialize(web_app))

    web_app.add_routes(routes)
    setup_swagger(web_app, swagger_url='/api')

    cors = aiohttp_cors.setup(web_app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        ),
    })

    for resource in web_app.router.resources():
        cors.add(resource)

    web_app.router.add_route('*', '/{tail:.*}', invalid_route)
    print("App ready")
    return web_app


app = create_app()
