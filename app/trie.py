from typing import Tuple, List

from app.model.city import CityModel


class Node:
    Id: int = 0

    def __init__(self):
        self.id = Node.Id
        Node.Id += 1
        self.final = False
        self.edges = {}
        self.cities = None

    def __str__(self):
        if len(self.edges) == 0:
            return self.id
        arr = []
        if self.final:
            arr.append("1")
        else:
            arr.append("0")

        for (label, node) in self.edges.items():
            arr.append(label)
            arr.append(str(node.id))

        return "_".join(arr)

    def __hash__(self):
        return self.__str__().__hash__()

    def __eq__(self, other):
        return self.__str__() == other.__str__()


class Trie:
    def __init__(self):
        self._previous_word = ""
        self._root = Node()

    def insert(self, cities: List[CityModel]):
        for city in cities:
            word = city.name

            if word < self._previous_word:
                raise Exception("Error: Words must be inserted in alphabetical order.")

            if word == self._previous_word:
                node = self._root
                for l in word:
                    node = node.edges[l]
                node.cities.append(city.id)
                continue

            node = self._root
            common_prefix = 0
            for l in word:
                if l not in node.edges:
                    break
                common_prefix += 1
                node = node.edges[l]

            for letter in word[common_prefix:]:
                next_node = Node()
                node.edges[letter] = next_node
                node = next_node

            if not node.cities:
                node.cities = [city.id]
            else:
                node.cities.append(city.id)
            node.final = True
            self._previous_word = word

    def lookup(self, word) -> List[Tuple[List[int], int]]:
        node = self._root
        for letter in word:
            if letter not in node.edges:
                return []
            node = node.edges[letter]

        return self._recursive_children(node, 0)

    def _recursive_children(self, node: Node, depth: int) -> List[Tuple[List[int], int]]:

        if node.final:
            children = [(node.cities, depth)]
        else:
            children = []

        for k, v in node.edges.items():
            children.extend(self._recursive_children(v, depth + 1))

        return children
