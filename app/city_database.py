from typing import Dict, List

from app.model.city import CityModel


class CityDatabase:
    def __init__(self, cities: List[CityModel]):
        self._database: Dict[int, CityModel] = {}
        for city in cities:
            self._database[city.id] = city

    def get(self, city_id: int) -> CityModel:
        return self._database[city_id]
