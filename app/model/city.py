from __future__ import annotations
from dataclasses import dataclass
from typing import List, Tuple


@dataclass
class CityModel:
    name: str
    id: int
    coordinates: Tuple[float, float] = None
    admin: str = None
    country: str = None
    description: str = None

    @classmethod
    def new(cls, raw_input: List[str]) -> CityModel:
        city_id = int(raw_input[0])
        name = raw_input[1]
        latitude = float(raw_input[2]) if len(raw_input) > 2 else None
        longitude = float(raw_input[3]) if len(raw_input) > 3 else None
        coordinates = (latitude, longitude)
        admin = raw_input[4] if len(raw_input) > 4 else None
        country = raw_input[5] if len(raw_input) > 5 else None
        description = ", ".join([x for x in [name, admin, country] if x])

        return CityModel(
            id=city_id,
            name=name,
            coordinates=coordinates,
            admin=admin,
            country=country,
            description=description
        )
