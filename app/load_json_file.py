import json

import aiohttp


async def load_json_file(url: str):
    async with aiohttp.ClientSession() as session:
        response = await session.request('GET', url=url)
        response.raise_for_status()
        byte_array = await response.read()

        my_json = byte_array.decode('utf8')

    return json.loads(my_json)


