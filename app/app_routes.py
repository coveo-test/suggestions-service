import json
from aiohttp import web
from aiohttp.web_request import Request
from app.use_cases.suggestions import SuggestionRequest, SuggestionUseCase

routes = web.RouteTableDef()


def json_dump(x: any):
    return json.dumps(x, ensure_ascii=False)


async def invalid_route(request: Request):
    error = {
        'error': f"Invalid route: {request.raw_path}"
    }
    return web.json_response(status=404, body=json_dump(error))


@routes.get("/healthcheck")
async def elb_status(request: Request):
    return web.json_response(status=200, body=json_dump({'status': 'Ok'}))


@routes.get("/suggestions")
async def suggestions(request: Request):
    """
        ---
        description: This end-point returns large cities name suggestion with optional latitude and longitude to improve score accuracy.
        tags:
        - Suggestions
        parameters:
        - in: query
          name: q
          description: The partial (or complete) search term
          required: true
          schema:
            type: string
        - in: query
          name: latitude
          description: The latitude to improve suggestion scoring
          required: false
          schema:
            type: number
            format: float
        - in: query
          name: longitude
          description: The longitude to improve suggestion scoring
          required: false
          schema:
            type: number
            format: float
        - in: query
          name: extended
          description: Add extra information in the response
          required: false
          schema:
            type: boolean
        produces:
        - application/json
        responses:
          "200":
            description: Successful operation. Returns the suggestions
            content:
              application/json:
                schema:
                  type: object
                  properties:
                    suggestions:
                    type: array
                    items:
                      type: object
                      properties:
                        name:
                          type: string
                          example: Québec
                        latitude:
                          type: float
                          example: Québec
                        longitude:
                          type: float
                        score:
                          type: float
                    time:
                    type: float
            "400":
              description: invalid, missing or wrong type parameters
            "500":
              description: System error
        """
    req = SuggestionRequest.from_dict(request.query)
    use_case = SuggestionUseCase(request.app["trie"], request.app["database"], request.app['geo_distance'])
    response = await use_case.execute(req)

    if response:
        return web.json_response(response.value, status=200, dumps=json_dump)

    return web.json_response(status=response.status_code, body=json_dump(response.value))

