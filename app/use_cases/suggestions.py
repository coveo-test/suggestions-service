from __future__ import annotations

import time
from dataclasses import dataclass
from typing import Dict, List, Tuple, Union

from app.city_database import CityDatabase
from app.geodistance import GeoDistance
from app.trie import Trie
from app.use_cases.core import ValidRequestObject, RequestObject, UseCase, ResponseSuccess


@dataclass
class SuggestionRequest(ValidRequestObject):
    word: str
    coordinates: Union[Tuple[float, float], None]
    extended: bool

    @classmethod
    def from_dict(cls, args: Dict[str, any]) -> RequestObject:
        invalid_request = RequestObject()

        coordinates = None

        if 'q' not in args or len(args['q']) < 1:
            invalid_request.add_error('q', 'parameter is missing')

        if 'latitude' in args and 'longitude' in args:
            latitude = 0
            longitude = 0
            try:
                latitude = float(args['latitude'])
            except ValueError:
                invalid_request.add_error('latitude', 'is not a float')

            try:
                longitude = float(args['longitude'])
            except ValueError:
                invalid_request.add_error('longitude', 'is not a float')

            if not invalid_request.has_errors():
                # geopy doesn't wrap latitude values
                bounds = (-90, 90)
                latitude = ((latitude - bounds[0]) % (bounds[1] - bounds[0] + 1) + bounds[0])
                coordinates = (latitude, longitude)

        extended = 'extended' in args

        if invalid_request.has_errors():
            return invalid_request

        return SuggestionRequest(
            word=args['q'],
            coordinates=coordinates,
            extended=extended
        )


class SuggestionUseCase(UseCase):
    def __init__(self, trie: Trie, database: CityDatabase, geo_distance: GeoDistance):
        self._trie = trie
        self._database = database
        self._geo_distance = geo_distance

    @staticmethod
    def _normalize(lookup: List[Tuple[List[int], int]]) -> List[Tuple[List[int], float]]:
        if len(lookup) == 0:
            return []

        max_value = max(1, max(lookup, key=lambda x: x[1])[1])

        if max_value == 0:
            return [(x[0], 1.0) for x in lookup]

        return [(x[0], 1.0 - (x[1] / max_value)) for x in lookup]

    async def process_request(self, request: SuggestionRequest):

        if request.extended:
            start = time.time()

        trie_lookup = self._trie.lookup(request.word)
        lookup = self._normalize(trie_lookup)
        result = self._format_suggestions(lookup, request.coordinates, request.extended)

        if request.extended:
            end = time.time()
            result['time'] = (end - start) * 1000

        return ResponseSuccess(result)

    def _format_suggestions(self, raw_result: List[Tuple[List[int], float]],
                            coordinate: Tuple[float, float], extended: bool) -> any:

        if coordinate:
            scoring = lambda score, coord: round(score * self._geo_distance.normalized_distance(coordinate, coord), 7)
        else:
            scoring = lambda score, coord: round(score, 7)

        result: List[any] = []
        for ids, current_score in raw_result:
            for x in ids:
                city_model = self._database.get(x)

                obj = {
                    'name': city_model.description,
                    'latitude': city_model.coordinates[0],
                    'longitude': city_model.coordinates[1],
                    'score': scoring(current_score, city_model.coordinates),
                }

                if extended and coordinate:
                    distance = self._geo_distance.distance(coordinate, city_model.coordinates)
                    obj['distance'] = {'km': distance[0], 'mi': distance[1]}
                result.append(obj)
        return {
            'suggestions': sorted(result, key=lambda y: y['score'], reverse=True)
        }
