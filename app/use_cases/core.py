from __future__ import annotations
import logging
from typing import List, Tuple, Dict

logger = logging.getLogger(__name__)


class RequestObject(object):

    def __init__(self):
        self._errors: List[Tuple[str, str]] = []

    def add_error(self, parameter: str, message: str):
        self._errors.append((parameter, message))

    def has_errors(self):
        return len(self._errors) > 0

    @property
    def errors(self):
        return self._errors

    def __nonzero__(self):
        return False

    __bool__ = __nonzero__


class ValidRequestObject(RequestObject):

    @classmethod
    def from_dict(cls, args: Dict[str, any]):
        raise NotImplementedError

    def __nonzero__(self):
        return True

    __bool__ = __nonzero__


class Response(object):
    SUCCESS: int = 200
    RESOURCE_ERROR: int = 404
    PARAMETERS_ERROR: int = 400
    SYSTEM_ERROR: int = 500

    def __init__(self, success: bool, status_code: int, response_value: any):
        self._success: bool = success
        self._status_code: int = status_code
        self._response_value: any = response_value

    @property
    def status_code(self):
        return self._status_code

    @property
    def value(self) -> any:
        return self._response_value

    def __bool__(self) -> bool:
        return self._success


class ResponseSuccess(Response):

    def __init__(self, value: any = None):
        super().__init__(True, self.SUCCESS, value)


class ResponseFailure(Response):

    def __init__(self, status_code: int, message: str):
        value = self._format_message(message)
        super().__init__(False, status_code, value)

    @staticmethod
    def _format_message(message: str):
        if isinstance(message, Exception):
            return "{}: {}".format(message.__class__.__name__, "{}".format(message))
        return message

    @classmethod
    def build_resource_error(cls, message: any = None) -> ResponseFailure:
        return cls(cls.RESOURCE_ERROR, message)

    @classmethod
    def build_system_error(cls, message: any = None) -> ResponseFailure:
        return cls(cls.SYSTEM_ERROR, message)

    @classmethod
    def build_parameters_error(cls, message: any = None) -> ResponseFailure:
        return cls(cls.PARAMETERS_ERROR, message)

    @classmethod
    def build_from_invalid_request(cls, request: RequestObject):
        errors = {}
        for k, v in request.errors:
            errors[k] = v
        return cls.build_parameters_error(errors)


class UseCase(object):

    async def execute(self, request: RequestObject) -> Response:
        if not request:
            return ResponseFailure.build_from_invalid_request(request)
        try:
            return await self.process_request(request)
        except Exception as exc:
            logger.exception(exc)
            return ResponseFailure.build_system_error(
                "{}: {}".format(exc.__class__.__name__, "{}".format(exc)))

    async def process_request(self, request: RequestObject):
        raise NotImplementedError(
            "process_request() not implemented by UseCase class")
