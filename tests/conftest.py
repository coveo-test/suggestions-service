import asyncio

import pytest
from aiohttp.test_utils import TestServer

from tests.server.dummy_server import create_dummy_server


@pytest.yield_fixture
def event_loop():
    """Create an instance of the default event loop for each test case."""
    policy = asyncio.get_event_loop_policy()
    res = policy.new_event_loop()
    res._close = res.close
    res.close = lambda: None

    yield res

    res._close()


@pytest.yield_fixture()
def aiohttp_server(event_loop):
    """Factory to create a TestServer instance, given an app.

    aiohttp_server(app, **kwargs)
    """
    servers = []

    async def go(app, *, port=None, **kwargs):
        server = TestServer(app, port=port)
        await server.start_server(loop=event_loop, **kwargs)
        servers.append(server)
        return server

    yield go

    async def finalize():
        while servers:
            await servers.pop().close()

    event_loop.run_until_complete(finalize())


@pytest.yield_fixture(name='dummy_server')
async def _fix_dummy_server(aiohttp_server):
    return await create_dummy_server(aiohttp_server)
