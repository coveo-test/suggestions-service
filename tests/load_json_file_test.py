import json
from json import JSONDecodeError

import pytest
from aiohttp import ClientResponseError, InvalidURL

from app.load_json_file import load_json_file


@pytest.mark.asyncio
async def test_load_data_source_use_case_error_on_invalid_request():
    with pytest.raises(InvalidURL):
        await load_json_file("")


@pytest.mark.asyncio
async def test_load_data_source_use_case_error_on_401_unauthorized_url(dummy_server):
    with pytest.raises(ClientResponseError):
        await load_json_file(f"{dummy_server.app['server_name']}/401")


@pytest.mark.asyncio
async def test_load_data_source_use_case_error_on_403_forbidden_url(dummy_server):
    with pytest.raises(ClientResponseError):
        await load_json_file(f"{dummy_server.app['server_name']}/403")


@pytest.mark.asyncio
async def test_load_data_source_use_case_error_on_404_not_found_url(dummy_server):
    with pytest.raises(ClientResponseError):
        await load_json_file(f"{dummy_server.app['server_name']}/404")


@pytest.mark.asyncio
async def test_load_data_source_use_case_error_on_invalid_data_format(dummy_server):
    with pytest.raises(JSONDecodeError):
        await load_json_file(f"{dummy_server.app['server_name']}/textfile")


@pytest.mark.asyncio
async def test_load_data_source_use_case_loads_json_from_file(dummy_server):
    response = await load_json_file(f"{dummy_server.app['server_name']}/jsonfile")

    assert response == json.loads('{"mykey": "my_value"}')





