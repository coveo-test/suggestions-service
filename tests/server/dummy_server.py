from aiohttp import web
from aiohttp.web_response import Response


async def return_200(request):
    return web.json_response({'status': "OK"})


async def return_401(request):
    return Response(status=401)


async def return_403(request):
    return Response(status=403)


async def return_404(request):
    return Response(status=404)


async def return_json_file(request):
    with open('./tests/server/json_file_test.json', 'r') as fd:
        body = fd.read()
    return Response(status=200, body=body, headers={"Content-Disposition": "attachment; filename=./tests/server/json_file_test.json"})


async def return_text_file(request):
    with open('./tests/server/text_file_test.txt', 'r') as fd:
        body = fd.read()
    return Response(status=200, body=body, headers={"Content-Disposition": "attachment; filename=./tests/server/text_file_test.txt"})


async def create_dummy_server(create_server):
    app = web.Application()
    app.add_routes([
        web.get('/200', return_200),
        web.get('/401', return_401),
        web.get('/403', return_403),
        web.get('/404', return_404),
        web.get('/jsonfile', return_json_file),
        web.get('/textfile', return_text_file),
    ])
    server = await create_server(app)
    app.update(
        log=[],
        post_data={},
        server_name=f'http://localhost:{server.port}',
    )
    return server
