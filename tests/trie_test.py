import pytest

from app.model.city import CityModel
from app.trie import Trie


def test_trie_inserted_non_alphabetical_words_throws():
    cities = [CityModel('apples', 0), CityModel('apple', 1), ]
    trie = Trie()
    with pytest.raises(Exception):
        trie.insert(cities)


def test_trie_lookup_word():
    cities = [CityModel('apple', 0), CityModel('apples', 1), CityModel('appletree', 2)]
    expected_result = [([0], 1), ([1], 2), ([2], 5)]
    trie = Trie()
    trie.insert(cities)
    lookup = trie.lookup('appl')
    assert lookup == expected_result


def test_trie_lookup_duplicate_word():
    cities = [CityModel('apple', 0), CityModel('apple', 3), CityModel('apples', 1), CityModel('appletree', 2)]
    expected_result = [([0, 3], 1), ([1], 2), ([2], 5)]
    trie = Trie()
    trie.insert(cities)
    lookup = trie.lookup('appl')
    assert lookup == expected_result
