import pytest

from app.use_cases.core import RequestObject, ValidRequestObject, Response, ResponseSuccess, ResponseFailure, UseCase


def test_request_object_default_to_invalid():
    request = RequestObject()
    assert not request


def test_request_object_has_error_is_false_before_add_error():
    request = RequestObject()
    assert not request.has_errors()


def test_request_object_has_error_is_true_after_add_error():
    request = RequestObject()
    request.add_error("paramA", "valueB")
    assert request.has_errors()


def test_request_object_error_list_updated_by_add_error():
    request = RequestObject()
    param_name = "paramA"
    value_name = "valueB"

    request.add_error(param_name, value_name)
    assert request.errors.__len__() == 1
    assert request.errors[0][0] == param_name
    assert request.errors[0][1] == value_name


def test_valid_request_object_throws_not_implemented():
    request = ValidRequestObject()
    with pytest.raises(NotImplementedError):
        request.from_dict({})


def test_response_bool_is_false_on_failure():
    is_success = False
    response = Response(is_success, 1, None)
    assert not response


def test_response_bool_is_true_on_success():
    is_success = True
    response = Response(is_success, 1, None)
    assert response


def test_response_status_code_is_updated_from_constructor():
    is_success = True
    status_code = 1
    response = Response(is_success, status_code, None)
    assert response.status_code == status_code


def test_response_response_is_updated_from_constructor():
    is_success = True
    status_code = 1
    value = "MyValue"
    response = Response(is_success, status_code, value)
    assert response.value == value


def test_response_success_is_true():
    response = ResponseSuccess()
    assert response


def test_response_success_default_value_is_none():
    response = ResponseSuccess()
    assert response.value is None


def test_response_success_status_code_is_set():
    response = ResponseSuccess()
    assert response.status_code == Response.SUCCESS


def test_response_failure_is_false():
    response = ResponseFailure(1, "")
    assert not response


def test_response_failure_status_code_is_updated():
    status_code = 1
    response = ResponseFailure(status_code, "")
    assert response.status_code == status_code


def test_response_failure_build_resource_error_status_code_is_set():
    response = ResponseFailure.build_resource_error()
    assert response.status_code == Response.RESOURCE_ERROR


def test_response_failure_build_system_error_status_code_is_set():
    response = ResponseFailure.build_system_error()
    assert response.status_code == Response.SYSTEM_ERROR


def test_response_failure_build_parameters_error_status_code_is_set():
    response = ResponseFailure.build_parameters_error()
    assert response.status_code == Response.PARAMETERS_ERROR


def test_response_failure_build_from_invalid_request_status_code_is_set():
    response = ResponseFailure.build_parameters_error()
    assert response.status_code == Response.PARAMETERS_ERROR


@pytest.mark.asyncio
async def test_use_case_failure_response_on_invalid_request():
    request = RequestObject()
    use_case = UseCase()
    response = await use_case.execute(request)
    assert not response


@pytest.mark.asyncio
async def test_use_case_system_failure_on_exception():
    request = ValidRequestObject()
    use_case = UseCase()
    response = await use_case.execute(request)
    assert not response
    assert response.status_code == Response.SYSTEM_ERROR
