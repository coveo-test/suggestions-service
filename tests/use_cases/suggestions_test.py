import pytest

from app.city_database import CityDatabase
from app.geodistance import GeoDistance
from app.model.city import CityModel
from app.trie import Trie
from app.use_cases.suggestions import SuggestionRequest, SuggestionUseCase


def test_suggestions_request_default_on_missing_query_string():
    request = SuggestionRequest.from_dict({})
    assert not request
    errors = [x[0] for x in request.errors]
    assert 'q' in errors

    request = SuggestionRequest.from_dict({'q': ""})
    assert not request
    errors = [x[0] for x in request.errors]
    assert 'q' in errors


def test_suggestions_request_use_provided_query_string():
    expected = "myValue"
    request = SuggestionRequest.from_dict({'q': expected})
    assert request.word == expected


def test_suggestions_request_wrap_latitude():
    request = SuggestionRequest.from_dict({'q': "something", 'latitude': '91', 'longitude': '0'})
    assert request.coordinates == (-90, 0)

    request = SuggestionRequest.from_dict({'q': "something", 'latitude': '-91', 'longitude': '0'})
    assert request.coordinates == (90, 0)

    request = SuggestionRequest.from_dict({'q': "something", 'latitude': '135', 'longitude': '0'})
    assert request.coordinates == (-46, 0)


def test_suggestions_request_error_on_non_float_coords():
    request = SuggestionRequest.from_dict({'q': "something", 'latitude': 'wrong_value', 'longitude': 'wrong_value'})
    assert not request
    errors = [x[0] for x in request.errors]
    assert 'latitude' in errors
    assert 'longitude' in errors


@pytest.mark.asyncio
async def test_suggestions_use_case_error_on_invalid_request():
    cities = []
    trie = Trie()
    trie.insert(cities)
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())

    request = SuggestionRequest.from_dict({})
    response = await use_case.execute(request)
    assert not response

    request = SuggestionRequest.from_dict({'q': "something", 'latitude': 'wrong_value', 'longitude': 'wrong_value'})
    response = await use_case.execute(request)
    assert not response


@pytest.mark.asyncio
async def test_suggestions_use_case_return_values_from_trie():
    cities = [CityModel.new(['0', 'apple']), CityModel.new(['3', 'apple']), CityModel.new(['1', 'apples']), CityModel.new(['2', 'appletree'])]
    expected_result = {'suggestions': [
        {'name': 'apple', 'score': 1.0 - 1/5, 'latitude': None, 'longitude': None},
        {'name': 'apple', 'score': 1.0 - 1/5, 'latitude': None, 'longitude': None},
        {'name': 'apples', 'score': 1.0 - 2/5, 'latitude': None, 'longitude': None},
        {'name': 'appletree', 'score': 1.0 - 5/5, 'latitude': None, 'longitude': None}
        ]}

    trie = Trie()
    trie.insert(cities)

    request = SuggestionRequest.from_dict({'q': 'appl'})
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())
    response = await use_case.execute(request)

    assert response
    assert response.value == expected_result


@pytest.mark.asyncio
async def test_suggestions_use_case_return_values_from_trie_with_perfect_coordinates():
    cities = [CityModel.new(['0', 'apple', '50', '80']), CityModel.new(['3', 'apple', '50', '80']), CityModel.new(['1', 'apples', '50', '80']), CityModel.new(['2', 'appletree', '50', '80'])]
    expected_result = {'suggestions': [
        {'name': 'apple', 'score': 1.0 - 1/5, 'latitude': 50.0, 'longitude': 80.0},
        {'name': 'apple', 'score': 1.0 - 1/5, 'latitude': 50.0, 'longitude': 80.0},
        {'name': 'apples', 'score': 1.0 - 2/5, 'latitude': 50.0, 'longitude': 80.0},
        {'name': 'appletree', 'score': 1.0 - 5/5, 'latitude': 50.0, 'longitude': 80.0}
        ]}
    trie = Trie()
    trie.insert(cities)

    request = SuggestionRequest.from_dict({'q': 'appl', 'latitude': '50', 'longitude': '80'})
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())
    response = await use_case.execute(request)

    assert response
    assert response.value == expected_result


@pytest.mark.asyncio
async def test_suggestions_use_case_return_values_from_trie_with_coordinates():
    cities = [CityModel.new(['0', 'apple', '50', '80']), CityModel.new(['3', 'apple', '51', '81']), CityModel.new(['1', 'apples', '-50', '-80']), CityModel.new(['2', 'appletree', '-50', '13'])]
    expected_result = {'suggestions': [
        {'name': 'apple', 'score': 1.0 - 1 / 5, 'latitude': 50.0, 'longitude': 80.0},
        {'name': 'apple', 'score': 0.7947236, 'latitude': 51.0, 'longitude': 81.0},
        {'name': 'apples', 'score': 0.04247, 'latitude': -50.0, 'longitude': -80.0},
        {'name': 'appletree', 'score': 0.0, 'latitude': -50.0, 'longitude': 13.0}
    ]}
    trie = Trie()
    trie.insert(cities)

    request = SuggestionRequest.from_dict({'q': 'appl', 'latitude': '50', 'longitude': '80'})
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())
    response = await use_case.execute(request)

    assert response
    assert response.value == expected_result


@pytest.mark.asyncio
async def test_suggestions_use_case_return_no_values():
    cities = [CityModel.new(['0', 'apple', '50', '80']), CityModel.new(['3', 'apple', '51', '81']), CityModel.new(['1', 'apples', '-50', '-80']), CityModel.new(['2', 'appletree', '-50', '13'])]
    expected_result = {'suggestions': []}
    trie = Trie()
    trie.insert(cities)

    request = SuggestionRequest.from_dict({'q': 'bar', 'latitude': '50', 'longitude': '80'})
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())
    response = await use_case.execute(request)

    assert response
    assert response.value == expected_result


@pytest.mark.asyncio
async def test_suggestions_use_case_return_values_sorted_by_score():
    expected_result = {'suggestions': [
        {'name': 'London', 'score': 1, 'latitude': 51.50853, 'longitude': -0.12574},
        {'name': 'London', 'score': 0.7054398419039839, 'latitude': 42.98339, 'longitude': -81.23304},
        {'name': 'Londonderry County Borough', 'score': 0.0, 'latitude': 54.99721, 'longitude': -7.30917}
    ]}
    cities = [
        CityModel.new(['0', 'London', '42.98339', '-81.23304']),
        CityModel.new(['1', 'London', '51.50853', '-0.12574']),
        CityModel.new(['2', 'Londonderry County Borough', '54.99721', '-7.30917'])
    ]

    trie = Trie()
    trie.insert(cities)

    request = SuggestionRequest.from_dict({'q': 'London', 'latitude': '51.50853', 'longitude': '-0.12574'})
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())
    response = await use_case.execute(request)

    assert response
    assert response.value == expected_result


@pytest.mark.asyncio
async def test_suggestions_use_case_return_values_sorted_by_score():
    expected_result = {'suggestions': [
        {'name': 'Québec', 'score': 1, 'latitude': 46.81228, 'longitude': -71.21454}
    ]}
    cities = [
        CityModel.new(['0', 'Québec', '46.81228', '-71.21454'])
    ]

    trie = Trie()
    trie.insert(cities)

    request = SuggestionRequest.from_dict({'q': 'Québec', 'latitude': '46.81228', 'longitude': '-71.21454'})
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())
    response = await use_case.execute(request)

    assert response
    assert response.value == expected_result


@pytest.mark.asyncio
async def test_suggestions_use_case_return_extended_result():
    cities = [
        CityModel.new(['0', 'Québec', '46.81228', '-71.21454'])
    ]

    trie = Trie()
    trie.insert(cities)

    request = SuggestionRequest.from_dict({'q': 'Québec', 'latitude': '46.81228', 'longitude': '-71.21454', 'extended': True})
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())
    response = await use_case.execute(request)

    assert response
    assert 'time' in response.value

    # Executing the same request without the extended argument
    request = SuggestionRequest.from_dict({'q': 'Québec', 'latitude': '46.81228', 'longitude': '-71.21454'})
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())
    response = await use_case.execute(request)

    assert response
    assert 'time' not in response.value


@pytest.mark.asyncio
async def test_suggestions_use_case_extended_return_distance():
    cities = [
        CityModel.new(['0', 'Québec', '46.81228', '-71.21454'])
    ]
    trie = Trie()
    trie.insert(cities)

    request = SuggestionRequest.from_dict({'q': 'Québec', 'latitude': '0', 'longitude': '0', 'extended': True})
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())
    response = await use_case.execute(request)

    assert response
    assert 'distance' in response.value['suggestions'][0]

    request = SuggestionRequest.from_dict({'q': 'Québec', 'extended': False})
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())
    response = await use_case.execute(request)

    assert response
    assert 'distance' not in response.value['suggestions'][0]


@pytest.mark.asyncio
async def test_suggestions_no_error_on_longitude_without_latitude():
    cities = [
        CityModel.new(['0', 'Québec', '46.81228', '-71.21454'])
    ]
    trie = Trie()
    trie.insert(cities)

    request = SuggestionRequest.from_dict({'q': 'Québec', 'latitude': '0', 'extended': True})
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())
    response = await use_case.execute(request)

    assert response


@pytest.mark.asyncio
async def test_suggestions_no_error_on_latitude_without_longitude():
    cities = [
        CityModel.new(['0', 'Québec', '46.81228', '-71.21454'])
    ]
    trie = Trie()
    trie.insert(cities)

    request = SuggestionRequest.from_dict({'q': 'Québec', 'longitude': '0', 'extended': True})
    use_case = SuggestionUseCase(trie, CityDatabase(cities), GeoDistance())
    response = await use_case.execute(request)

    assert response

