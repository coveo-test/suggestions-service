FROM python:3.7.3-slim

WORKDIR /usr/src/app

COPY ./entry-point.sh /usr/src

COPY requirements.txt /usr/src/app
RUN pip install --no-cache-dir -r requirements.txt

COPY ./app/ /usr/src/app

WORKDIR /usr/src

CMD sh ./entry-point.sh